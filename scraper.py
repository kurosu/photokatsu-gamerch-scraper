import requests, urllib.request, os, bs4

def main():
    contentDir = "content"
    domain = "https://aikatsu-photo.gamerch.com"
    domainPage = requests.get(domain)
    domainSoup = bs4.BeautifulSoup(domainPage.content, "html.parser")

    rarityPages = domainSoup.find("h4", string="レア別").parent.find_next_sibling().find_all("a")

    for pageLink in rarityPages:
        rarityPageUrl = pageLink["href"]
        listPage = requests.get(rarityPageUrl)
        listSoup = bs4.BeautifulSoup(listPage.content, "html.parser")

        cardCells = listSoup.find(id="js_async_main_column_text").table.find_all("td", attrs={"data-col": "1"})
        for cell in cardCells:
            cardUrl = domain + cell.find("a")["href"]
            print(cardUrl)

            cardPage = requests.get(cardUrl)
            cardSoup = bs4.BeautifulSoup(cardPage.content, "html.parser")
            mainContent = cardSoup.find(id="js_async_main_column_text")

            cardId = getTitleTop("No.", mainContent)

            if not os.path.exists(contentDir):
                os.mkdir(contentDir)

            with open(contentDir + "/" + cardId + ".html", 'w') as htmlFile:
                htmlFile.write(str(mainContent))

def getTitleTop(title, soup):
    top = ""
    if soup.find("span", string=title):
        tags = soup.find("span", string=title).next_siblings
        for item in tags:
            if type(item) == bs4.element.NavigableString:
                top += item.strip()
            elif not item.has_attr("class") or "ui_bottom_detail" not in item.attrs["class"]:
                top += item.text.strip()
    return top.strip()

main()
