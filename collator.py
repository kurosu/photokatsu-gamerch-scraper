import re, csv, os, bs4, unicodedata

def main():
    if not os.path.exists("img/"):
        os.mkdir("img")

    with open("photokatsu.csv", 'w', newline='') as csvfile:
        fieldnames = [
                "title",
                "id",
                "rarity",
                "type",
                "character",
                "starting_aura",
                "maximum_aura",
                "appeal_points",
                "appeal",
                "skill",
                "skill_info",
                "brand",
                "brand_info",
                "date",
                "message1",
                "message2",
                "sibling_id",
                "filename",
                "img"
                ]

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for htmlFile in sorted(os.listdir("content")):
            with open("content/" + htmlFile, 'r') as htmlFileUnread:
                contents = htmlFileUnread.read()
                cardSoup = bs4.BeautifulSoup(contents, "html.parser")

                card = {}

                card["title"] = cardSoup.find("h2", id="js_wikidb_main_name").text

                card["id"] = getTitleTop("No.", cardSoup)
                card["rarity"] = getTitleTop("レア", cardSoup)
                card["type"] = getTitleTop("タイプ", cardSoup)
                card["character"] = getTitleTop("キャラクター", cardSoup)

                card["starting_aura"] = getTitleTop("初期オーラ", cardSoup)
                card["maximum_aura"] = getTitleTop("最大オーラ", cardSoup)
                card["appeal_points"] = getTitleTop("アピールポイント", cardSoup)

                card["appeal"] = getTitleTop("アピール", cardSoup)
                card["skill"] = getTitleTop("スキル", cardSoup)
                card["skill_info"] = getTitleBottom("スキル", cardSoup)

                card["brand"] = getTitleTop("ブランド", cardSoup)
                card["brand_info"] = getTitleBottom("ブランド", cardSoup)

                card["date"] = getTitleTop("実装日", cardSoup)

                card["message1"] = getTitleBottom("メッセージ1", cardSoup)
                card["message2"] = getTitleBottom("メッセージ2", cardSoup)

                if cardSoup.find("span", string="ドレスアップ後") and re.search(r"\d{4}", cardSoup.find("span", string="ドレスアップ後").parent.text):
                    card["sibling_id"] = re.search(r"\d{4}", cardSoup.find("span", string="ドレスアップ後").parent.text).group(0)
                elif cardSoup.find("span", string="入手方法") and re.search(r"\d{4}", cardSoup.find("span", string="入手方法").parent.text):
                    card["sibling_id"] = re.search(r"\d{4}", cardSoup.find("span", string="入手方法").parent.text).group(0)
                else:
                    card["sibling_id"] = ""

                card["img"] = cardSoup.find("img",class_="ui_wikidb_main_img")["src"]

                card["filename"] = "photokatsu_" + card["id"] + re.search(r"\.\w{3,}$",card["img"]).group(0)

                if not os.path.exists("img/" + card["filename"]):
                    urllib.request.urlretrieve(card["img"], "img/" + card["filename"])

                writer.writerow(card)
                print(card["id"] + " complete")

def getTitleTop(title, soup):
    top = ""
    if soup.find("span", string=title):
        tags = soup.find("span", string=title).next_siblings
        for item in tags:
            if type(item) == bs4.element.NavigableString:
                top += item.strip()
            elif not item.has_attr("class") or "ui_bottom_detail" not in item.attrs["class"]:
                top += item.text.strip()
    return unicodedata.normalize("NFKC", top.strip())

def getTitleBottom(title, soup):
    bottom = ""
    if soup.find("span", string=title):
        tags = soup.find("span", string=title).parent.find("span", class_="ui_bottom_detail").contents
        for item in tags:
            if type(item) == bs4.element.NavigableString:
                bottom += item.strip()
            elif item.name == "br":
                bottom += "\n"
            else:
                bottom += item.text
    return unicodedata.normalize("NFKC", bottom.strip())

main()
