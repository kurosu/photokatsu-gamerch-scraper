# photokatsu gamerch scraper

Aikatsu! Photo on stage!! scraper and scraped content.

- `scraper.py` scrapes the Photokatsu Gamerch wiki and saves important content.
- `collator.py` reads the downloaded content, saves images, and stores info in `photokatsu.csv`.

all info and images are already saved in this repo.
